package com.xa.extra.SoalHari5;

import java.util.Scanner;

public class soal4Main {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Masukan nilai siswa");
        String[] input = scan.nextLine().split(" ");
        int [] nilai = new int[input.length];
        for (int i =0; i < input.length; i++ ){
            nilai[i] = Integer.parseInt(input[i]);
        }

        int nilaiMax = findMax(nilai);

        System.out.println("Nilai tertinggi: "+nilaiMax);

        int countMax =0;
        for (int n: nilai){
            if(n == nilaiMax)
                countMax++;
        }
        System.out.println("nilai tertinggi ada: "+countMax);

    }

    public static int findMax(int[] nilai){
        int max =0;
        for (int n: nilai){
            if(n > max)
                max =n;
        }
        return max;
    }
}
