package com.xa.extra.SoalHari5;

import java.util.HashSet;

public class soal6Main {
    final static int n =7;
    public static void main (String[] args){

        int numA= 12;
        int numB =0;
        int idx =0;
        System.out.print("Bilangan himpunan A: ");
        HashSet<Integer> himpA = new HashSet<Integer>();
        for (int i =0; i< n; i++){
            System.out.print(numA+" ");
            himpA.add(numA);
            numA+=2;
        }
        System.out.println();
        HashSet<Integer> himpB = new HashSet<Integer>();
        System.out.print("Bilangan himpunan B: ");
        while ( idx < n){
            if(numB%7 ==3){
                System.out.print(numB+" ");
                himpB.add(numB);
                idx++;
            }
            numB++;
        }

        himpA.retainAll(himpB);
        System.out.println("\nIrisan bilangan himpunan A dan B adalah: "+himpA);

    }
}
