--soal no 2
ALTER TABLE tblm_dosen ALTER COLUMN nama_dosen TYPE character varying(200);

--soal no 3
select 		m.kode_mahasiswa, m.nama_mahasiswa, j.nama_jurusan, a.deskripsi
from 		tblm_mahasiswa m inner join tblm_jurusan j
on 			m.id_jurusan_fk = j.id_jurusan_pk
inner join 	tblm_agama a
on 			m.id_agama_fk = a.id_agama_pk
where 		m.kode_mahasiswa = 'M0001';

--soal no 4
select 		m.kode_mahasiswa, m.nama_mahasiswa, j.nama_jurusan,
case 		j.is_active
			when '1' then 'aktif'
			when '0' then 'Non-Aktif'
end as 		status_jurusan
from 		tblm_mahasiswa m  
inner join 	tblm_jurusan j
on 			m.id_jurusan_fk = j.id_jurusan_pk
where 		j.is_active = '0'


-- soal no 5
select 		m.kode_mahasiswa, m.nama_mahasiswa,u.nama_ujian, n.nilai, 
case 		u.is_active
			when '1' then 'aktif'
			when '0' then 'Non-Aktif'
end as 		status_ujian
from 		tblt_nilai n  
inner join 	tblm_mahasiswa m
on 			n.id_mahasiswa_fk = m.id_mahasiswa_pk
inner join	tblr_ujian u
on			n.id_ujian_fk = u.id_ujian_pk
where		n.nilai >80 and u.is_active = '1'
order by 	m.kode_mahasiswa asc

where n.nilai > 80 and n.is_active = '1'
order by m.kode_mahasiswa asc

--soal no 6
select 		* from tblm_jurusan
where 		nama_jurusan ilike '%sistem%'
order by 	id_jurusan_pk asc

--soal no 7
select 		m.nama_mahasiswa, count (n.id_ujian_fk) as total_ujian
from 		tblt_nilai n
inner join 	tblm_mahasiswa m
on 			n.id_mahasiswa_fk = m.id_mahasiswa_pk
group by 	m.nama_mahasiswa
having count (n.id_ujian_fk) > 1

--soal no 8
create view dosen_mahasiswa As -- untuk no 9
select 		m.kode_mahasiswa, m.nama_mahasiswa, j.nama_jurusan,
			a.deskripsi as agama, d.nama_dosen, 
			CASE j.is_active				
			when '1' then 'Aktif'
			when '0' then 'Non-Aktif'
			end as status_jurusan,
			t.deskripsi
from 		tblm_mahasiswa m
inner join	tblm_jurusan j
on			m.id_jurusan_fk = j.id_jurusan_pk
inner join	tblm_agama a
on			m.id_agama_fk = a.id_agama_pk
inner join 	tblm_dosen d
on 			d.id_jurusan_fk = j.id_jurusan_pk
inner join 	tblm_type_dosen t
on			d.id_type_dosen_fk = t.id_type_dosen_pk
where kode_mahasiswa = 'M0001'

--soal no 9
select * from dosen_mahasiswa

--soal no 10
select m.kode_mahasiswa, m.nama_mahasiswa, n.nilai, n.is_active as status_jurusan
from tblt_nilai n  right outer join tblm_mahasiswa m
on n.id_mahasiswa_fk = m.id_mahasiswa_pk



