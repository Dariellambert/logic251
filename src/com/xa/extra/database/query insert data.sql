insert into tblm_agama (kode_agama, deskripsi, is_active, created_by, 
						created_date, updated_by, updated_date) values
			('A0001', 'Islam','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('A0002', 'Kristen','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('A0003', 'Katolik','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('A0004', 'Hindu','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('A0005', 'Budha','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873');
			
insert into tblm_type_dosen (kode_type_dosen, deskripsi, is_active, created_by, 
						created_date, updated_by, updated_date) values
			('T0001', 'Tetap','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('T0002', 'Honorer','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('T0003', 'Expertise','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873');
			
insert into tblm_jurusan (kode_jurusan, nama_jurusan, is_active, created_by, 
						created_date, updated_by, updated_date) values
			('J0001', 'Teknik Informatika','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('J0002', 'Management Informatika','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('J0003', 'Sistem Informasi','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('J0004', 'Sistem Komputer','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('J0005', 'Komputer Akuntansi','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873');
			
insert into tblm_dosen (kode_dosen, nama_dosen, id_jurusan_fk, id_type_dosen_fk, is_active, created_by, 
						created_date, updated_by, updated_date) values
			('D0001', 'Prof. Dr. Retno Wahyuningsih', 1, 2 ,'1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('D0002', 'Prof. Roy M. Sutikno', 2, 1,'1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('D0003', 'Prof. Hendri A. Verburgh', 3,2, '1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('D0004', 'Prof. Risma Suparwata', 4,2,'1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('D0005', 'Prof. Amir Sjarifuddin Madjidm MM, MA', 5,1, '1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873');
			
insert into tblm_mahasiswa (kode_mahasiswa, nama_mahasiswa, alamat, id_agama_fk, id_jurusan_fk, is_active, created_by, 
						created_date, updated_by, updated_date) values
			('M0001', 'Budi Gunawan', 'Jl. Mawar No. 3 RT 05 Cicalengka, Bandung', 1, 1 ,'1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('M0002', 'Rinto Raharjo', 'Jl. Kebagusan No. 33 RT 04 RW 06 Bandung', 1, 2,'1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('M0003', 'Asep Saepudin', 'Jl. Sumatera No. 12 RT 02 RW 01, Ciamis', 1,3, '1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('M0004', 'M. Hafif Isbullah', 'Jl. Jawa No. 01 RT 01 RW 01, Jakarta Pusat', 2,1,'1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('M0005', 'Cahyono','JL. Niagara No 54 RT 01 RW 09, Surabaya', 3,2, '1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873');
			
insert into tblr_ujian (kode_ujian, nama_ujian, is_active, created_by, 
						created_date, updated_by, updated_date) values
			('U0001', 'Algoritma','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('U0002', 'Aljabar','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('U0003', 'Statistika','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('U0004', 'Etika Profesi','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			('U0005', 'Bahasa Inggris','1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873');
			
insert into tblt_nilai (id_mahasiswa_fk, id_ujian_fk, nilai, is_active, created_by, 
						created_date, updated_by, updated_date) values
			(4, 1, 90, '1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			(1, 1, 80, '1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			(2, 3, 85, '1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			(4, 2, 95, '1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873'),
			(5, 5, 70, '1', 'adit', '2019-10-30 13:48:49.873', 'adit', '2019-10-30 13:48:49.873');
			
