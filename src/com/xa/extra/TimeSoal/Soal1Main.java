package com.xa.extra.TimeSoal;

import java.util.Scanner;

public class Soal1Main {

    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        String[]  masuk = scan.nextLine().split(":");
        String[] keluar = scan.nextLine().split(":");



        int biaya = hitungBiaya(masuk,keluar);
        System.out.println("Biaya parkir adalah: "+biaya);
    }

    public static int hitungBiaya(String[] masuk, String[] keluar){
        int jamMasuk = Integer.parseInt(masuk[0]);
        int menitMasuk = Integer.parseInt(masuk[1]);

        int jamKeluar = Integer.parseInt(keluar[0]);
        int menitKeluar = Integer.parseInt((keluar[1]));

        int selisihJam = jamKeluar - jamMasuk;
        int selisiMenit = menitKeluar - menitMasuk;

        int biaya =0;

        if( menitKeluar < menitMasuk){
            biaya = ((selisihJam-1)*3000) + ((selisiMenit+60)*50);
        }
        else {
            biaya = (selisihJam*3000) + (selisiMenit*50);
        }
        return biaya;
    }
}

