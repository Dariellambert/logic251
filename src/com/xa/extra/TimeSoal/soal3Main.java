package com.xa.extra.TimeSoal;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class soal3Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukan tanggal pinjam: ");
        String tglPinjam = scan.nextLine();
        System.out.print("Masukan tanggal kembali: ");
        String tglKembali = scan.nextLine();


        String dateBeforeString = changeDateFormat(tglPinjam);
        String dateAfterString = changeDateFormat(tglKembali);
        //Parsing the date
        LocalDate dateBefore = LocalDate.parse(dateBeforeString);
        LocalDate dateAfter = LocalDate.parse(dateAfterString);

        //calculating number of days in between
        long noOfDaysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);

        //displaying the number of days
        System.out.println("Dendanya adalah: "+(noOfDaysBetween-3)*500);
    }

    public static String changeDateFormat(String tanggal){
        String[] pisah = tanggal.split("-");
        String temp = pisah[0];
        pisah[0] = pisah[pisah.length-1];
        pisah[pisah.length-1] = temp;
        String newFormat = pisah[0]+"-"+pisah[1]+"-"+pisah[2];
        return newFormat;
    }
}
