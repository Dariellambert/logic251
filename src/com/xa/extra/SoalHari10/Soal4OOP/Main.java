package com.xa.extra.SoalHari10.Soal4OOP;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);

        Pelanggan dina = new Pelanggan("Dina");
        Rental film1 = new Rental("X-men Dark Phoenix");
        Rental film2 = new Rental("Haikyuu Session 1");
        Rental film3 = new Rental("Friend Zone");
        Rental film4 = new Rental("X-Men Days Of Future Past");
        Rental film5 = new Rental("Hitman Agent 47");
        Rental film6 = new Rental("Kingsman The Secret Service");

        dina.pinjam(film1,"10 Aug 2019",7);
        dina.pinjam(film2,"10 Aug 2019", 14);
        dina.pinjam(film3,"10 Aug 2019", 5);
        dina.pinjam(film4,"10 Sep 2019",7);
        dina.pinjam(film5,"10 Sep 2019", 14);
        dina.pinjam(film6,"10 Sep 2019", 5);
        dina.showDaftarRental();
        System.out.println();
        dina.kembalikan(film3, "17 Sep 2019");
        //dina.kembalikan(film5, "17 Sep 2019");
        dina.kembalikan(film6, "17 Sep 2019");
        System.out.println();
        dina.showDaftarRental();
        System.out.println();

        System.out.println("Hari ini");
        dina.hitungDenda("18 Sep 2019");// misalkan hari ini tgl 18 sep 2019

        System.out.println("\n");
        System.out.print("Hitung denda pada tanggal: ");
        String input = scan.nextLine();
        dina.hitungDenda(input);




    }
}
