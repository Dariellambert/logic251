package com.xa.extra.SoalHari10.Soal4OOP;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

public class Pelanggan {
    private String nama;
    private static HashMap<String, Long> daftarRental = new HashMap<>();
    private static HashMap<String, String> tanggalRental= new HashMap<>();
    static DateTimeFormatter formatJam = DateTimeFormatter.ofPattern("dd MMM yyyy");

    Pelanggan(String nama){
        this.setNama(nama);
    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }



    public void pinjam(Rental rent, String tanggal, long durasi){
        daftarRental.put(rent.getFilm(), durasi);
        tanggalRental.put(rent.getFilm(), tanggal);
    }

    public void kembalikan(Rental rent, String tanggal){
        LocalDate tglKembali = LocalDate.parse(tanggal, formatJam);
        LocalDate tglPinjam = LocalDate.parse(tanggalRental.get(rent.getFilm()), formatJam);

        long deadline= daftarRental.get(rent.getFilm());
        long durasiPinjam = ChronoUnit.DAYS.between(tglPinjam, tglKembali);
        long denda = (durasiPinjam - deadline)*2000;

        if(durasiPinjam > deadline ){
            System.out.println("denda yang harus dibayar untuk film " + rent.getFilm()+" adalah: "+ denda);
            daftarRental.remove(rent.getFilm());
            tanggalRental.remove(rent.getFilm());
        }
        else{
            System.out.println("Terima kasih sudah mengembalikan film "+ rent.getFilm()+" tepat waktu");
            daftarRental.remove(rent.getFilm());
            tanggalRental.remove(rent.getFilm());
        }
    }

    public void hitungDenda(String tanggal){
        String[] tempFilm = new String[daftarRental.size()];
        String [] tempTanggal = new String[tanggalRental.size()];
        long [] tempDeadline = new long[tanggalRental.size()];


        int idx1=0;
        for (String i: daftarRental.keySet()){
            tempFilm[idx1] = i;
            idx1++;
        }

        int idx2=0;
        for (String i: tanggalRental.values()){
            tempTanggal[idx2] = i;
            idx2++;
        }

        int idx3=0;
        for (Long i: daftarRental.values()){
            tempDeadline[idx3] = i;
            idx3++;
        }

        long[] durasiPinjam2 = new long[tempTanggal.length];
        LocalDate tglKembali = LocalDate.parse(tanggal, formatJam);
        long[] denda = new long[tempTanggal.length];
        for (int i =0; i<tempTanggal.length;i++){
            LocalDate tglPinjam = LocalDate.parse(tempTanggal[i], formatJam);
            durasiPinjam2[i] = ChronoUnit.DAYS.between(tglPinjam, tglKembali);
            denda[i] = (durasiPinjam2[i]-tempDeadline[i])*2000;

        }
        String column1Format = "%-30.30s";
        String column2Format = "%10.10s";
        String formatInfo = column1Format + " | " + column2Format;

        System.out.println("tanggal "+tanggal);
        System.out.println("====Denda film yang belum dikembalikan=====");
        System.out.println("Nama film                             Denda");
        int idx4 =0;
        int total =0;
        for (String i: daftarRental.keySet()){
            if (denda[idx4] > 0){
                System.out.format(formatInfo, i, denda[idx4]);
                total += denda[idx4];
                idx4++;
                System.out.println();
            }
            else{
                System.out.format(formatInfo, i, "0");
                idx4++;
                System.out.println();
            }
        }
        System.out.println("____________________________________________");
        System.out.format(formatInfo, "total: ", total);
    }

    public void showDaftarRental(){
        String column1Format = "%-30.30s";
        String column2Format = "%10.3s";

        System.out.println("============Film yang dipinjam=============");
        String formatInfo = column1Format + " | " + column2Format;
        System.out.println("Nama film                      Durasi(Hari)");
        for (String i: daftarRental.keySet()){
            System.out.format(formatInfo, i, daftarRental.get(i));
            System.out.println();
        }

    }
}
