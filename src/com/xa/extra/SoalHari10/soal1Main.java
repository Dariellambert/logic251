package com.xa.extra.SoalHari10;

import java.util.Scanner;

public class soal1Main {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Masukan deret angka: ");
        String [] deretAngka = scan.nextLine().split(",");
        System.out.print("Masukan bilangan penambah: ");
        int penambah = scan.nextInt();
        int[] num = new int[deretAngka.length];
        for (int i  =0; i < deretAngka.length; i++){
            num[i] = Integer.parseInt(deretAngka[i]);
        }
        int numMax = findMax(num);
        int numMin = findMin(num);

        System.out.println(penambah+numMax);
        System.out.println(penambah+numMin);

    }

    public static int findMax(int[] num){
        int max = 0;
        for (int n: num){
            if (n > max)
                max =n;
        }
        return max;
    }

    public static int findMin(int[] num){
        int min = 0;
        for (int n: num){
            if (n < min)
                min =n;
        }
        return min;
    }
}
