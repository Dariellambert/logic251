package com.xa.pr;

public class Main {
    final static int n = 7;

    public static void main(String[] args) {
	// write your code here
        soal01();
        System.out.println("\n");
        soal02();
        System.out.println("\n");
        soal03();
        System.out.println("\n");
        soal04();
        System.out.println("\n");
        soal05();
        System.out.println("\n");
        soal06();
        System.out.println("\n");
        soal07();
        System.out.println("\n");
        soal08();
        System.out.println("\n");
        soal09();
        System.out.println("\n");
        soal10();
        System.out.println("\n");

    }

    public static void soal01(){
        int num = 1;
        System.out.println("Soal 01:");
        for (int i=0; i < n; i++){
            System.out.print(num+" ");
            num+=2;
        }
    }

    public static void soal02(){
        int num = 2;
        System.out.println("Soal 02:");
        for (int i=0; i < n; i++){
            System.out.print(num+" ");
            num+=2;
        }
    }

    public static void soal03(){
        int num = 1;
        System.out.println("Soal 03:");
        for (int i=0; i < n; i++){
            System.out.print(num+" ");
            num+=3;
        }
    }

    public static void soal04(){
        int num = 1;
        System.out.println("Soal 04:");
        for (int i=0; i < n; i++){
            if ((i+1) %3 == 0){
                System.out.print("* ");
            }
            else{
                System.out.print(num+" ");
                num+=4;
            }
        }
    }

    public static void soal05(){
        int num = 1;
        System.out.println("Soal 05:");
        for (int i=0; i < n; i++){
            if ((i+1) % 3 == 0){
                System.out.print("* ");
                num+=4;
            }
            else{
                System.out.print(num+" ");
                num+=4;
            }
        }
    }

    public static void soal06(){
        int num = 2;
        System.out.println("Soal 06:");
        for (int i=0; i < n; i++){
            System.out.print(num+" ");
            num*=2;
        }
    }

    public static void soal07(){
        int num = 3;
        System.out.println("Soal 07:");
        for (int i=0; i < n; i++){
            System.out.print(num+" ");
            num*=3;
        }
    }

    public static void soal08(){
        int num = 4;
        System.out.println("Soal 08:");
        for (int i=0; i < n; i++){
            if ((i+1)% 3 == 0){
                System.out.print("* ");
            }
            else{
                System.out.print(num+" ");
                num*=4;
            }
        }
    }

    public static void soal09(){
        int num = 3;
        System.out.println("Soal 09:");
        for (int i=0; i < n; i++){
            if ((i+1) % 4 == 0){
                System.out.print("XXX ");
                num*=3;
            }
            else{
                System.out.print(num+" ");
                num*=3;
            }
        }
    }

    public static void soal10(){
        int count,h=0,i=0,j;
        System.out.println("Soal 10:");
        while(h<n){
            j=0;
            count=0;
            while((j+1)<=(i+1)){
                if((i+1)%(j+1)==0)
                    count++;
                j++;
            }
            if(count==2){
                System.out.print((i+1)+ " ");
                h++;
            }
            i++;
        }
    }


}
