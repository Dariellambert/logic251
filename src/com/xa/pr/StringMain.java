package com.xa.pr;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class StringMain {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        //System.out.println(soal05(scan));
        //soal05(scan);
        //soal10(scan);
        soal08(scan);
        //soal10alt(scan);
    }

    public static void soal05(Scanner scan){
        String hack= "hackerrank";
        System.out.print("Masukan: ");//hereiamstackerrank
        String kata= scan.nextLine();

        char[] charHack  = hack.toCharArray();
        Arrays.sort(charHack);
        String hackSorted = new String(charHack);
        System.out.println(hackSorted); // sort kata hackerrank

        char[] charKata  = kata.toCharArray();
        Arrays.sort(charKata);
        String kataSorted = new String(charKata);
        System.out.println(kataSorted); //sort kata inputan

        int iter =0;
       for (int i =0; i < kataSorted.length(); i++){
            if (iter < hackSorted.length() && kataSorted.charAt(i) == hackSorted.charAt(iter))
                iter++;
        } //membandingkan kata inputan dan hacckerank

        if(iter == hackSorted.length()) // jika yang ketemu sama dengan length hackerrank
            System.out.print("\nYES");
        else
            System.out.println("NO");
    }

    public static void soal10(Scanner scan){
        System.out.print("Masukan Kata 1 : ");
        String kata1= scan.nextLine();
        System.out.print("Masukan Kata 2: ");
        String kata2= scan.nextLine();

        Set<Character> kata1Set = new HashSet<>();
        Set<Character> kata2Set = new HashSet<>();

        for(int i = 0; i < kata1.length(); i++)
            kata1Set.add(kata1.charAt(i));

        for(int i = 0; i < kata2.length(); i++)
            kata2Set.add(kata2.charAt(i));

        kata1Set.retainAll(kata2Set);

        System.out.println(kata1Set);

        if(kata1Set.size() > 0)
            System.out.println("YES");
        else
            System.out.println("NO");

    }

    public static void soal10alt(Scanner scan){
        System.out.print("Masukan Kata 1 : ");
        String kata1= scan.nextLine();
        System.out.print("Masukan Kata 2: ");//hereiamstackerrank
        String kata2= scan.nextLine();

        String max =" ";
        String min = " ";

        if (kata1.length() > kata2.length()){
            max = kata1;
            min = kata2;
        }
        else{
            max = kata2;
            min = kata1;
        }

        int countSama = 0;
        for (int i = 0; i < min.length(); i++){
            if (min.charAt(i) == max.charAt(i)){
                countSama++;
            }
        }

        if(countSama > 0)
            System.out.println("YES");
        else
            System.out.println("NO");

    }

    public static void soal08(Scanner scan){
        String s1=scan.nextLine();
        String s2=scan.nextLine();
        int cArr[]=new int[26];
        int cArr1[]=new int[26];

        for(int i=0;i<s1.length();i++){
            cArr[s1.charAt(i)-97]++;
            System.out.print(cArr[i]);
        }
        System.out.println("\n");

        for(int i=0;i<s2.length();i++){
            cArr1[s2.charAt(i)-97]++;
            System.out.print(cArr1[i]);
        }


        int count=0;
        for(int i=0;i<26;i++){
            count+=Math.abs(cArr[i]-cArr1[i]);
        }
        System.out.println("\n"+count);
    }

}
