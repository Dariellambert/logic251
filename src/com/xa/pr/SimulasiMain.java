package com.xa.pr;

import java.util.*;
import java.util.regex.*;

public class SimulasiMain {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //soal01(scan);
        //soal02(scan);
        soal05(scan);
        //soal06(scan);
    }

    public static void soal01 (Scanner scan) {
        int input1, input2, input3;
        Random rn = new Random();
        ArrayList<Integer> angka = new ArrayList<Integer>();

        System.out.print("Input: ");
        input1 = scan.nextInt();
        System.out.print("Input: ");
        input2 = scan.nextInt();
        input3 = rn.nextInt(10)+ 1;
        System.out.println("random: "+ input3);

        angka.add(input1);
        angka.add(input2);
        angka.add(input3);

        Integer max = Collections.max(angka);
        System.out.println("Output: "+max);


    }

    public static void soal02 (Scanner scan) {
        String password ="";
        int strength = 0;

        System.out.println("Masukan Password: ");
        password = scan.nextLine();
        if(password.length() == 6)
            strength++;
        if(password.charAt(0) >= 65 && password.charAt(0) <=90)
        if( password.matches("(?=.*[0-9]).*") )
            strength++;
        if( password.matches("(?=.*[a-z]).*") ||  password.matches("(?=.*[A-Z]).*")  )
            strength++;
        if( password.matches("(?=.*[A-Z]).*") )
            strength++;
        if( password.matches("(?=.*[~!@#$%^&*()_-]).*") )
            strength++;


        System.out.println("kekuatan sandi: " +strength);
    }

    public static void soal05 (Scanner scan){
        System.out.print("Masukan kalimat: ");
        String input = scan.nextLine().toLowerCase();

        char [] tempVowel = new char[input.length()];
        char [] tempConsonant = new char[input.length()];
        for (int i = 0; i < input.length(); i++){
            if("aeiou".indexOf(input.charAt(i)) < 0){
                if(input.charAt(i)>= 97 && input.charAt(i)<= 122){
                    tempConsonant[i] = input.charAt(i);
                }
            }
            else {
                tempVowel[i] = input.charAt(i);
            }

        }
        char [] consonantSorted = bubbleSortCharAsc(tempConsonant);
        char [] vowelSorted = bubbleSortCharDesc(tempVowel);

        String consonant = new String(consonantSorted);
        String vowel = new String(vowelSorted);

        System.out.println(vowel);
        System.out.println(consonant);
    }

    public static void soal06 (Scanner scan){
        int input = scan.nextInt();

        int temp = 0;
        int [] num = new int[input];
        int [] ganjil = new int[num.length];
        int [] genap = new  int[num.length];

        for (int i =0; i < input; i++){
            temp++;
            num[i] = temp;
           // System.out.print(num[i]+ " ");
        }

        int idx1 = 0;
        int idx2 = 1;
        for (int i =0; i < num.length; i++){
            if ( num[i] %2 == 1){
                ganjil[i] = num[i];
                idx1+=2;
            }
            else if (num[i]% 2 == 0){
                genap[i] = num[i];
                idx2+=2;
            }
            //System.out.print(+ganjil[i]);
        }

        for (int i = 0; i < genap.length; i+=2){
            System.out.print(genap[i+1]+" ");
        }
        System.out.println("");
        for (int i = 0; i < ganjil.length; i+=2){
            System.out.print(ganjil[i]+" ");
        }
        //
    }

    public static char[] bubbleSortCharAsc(char[] ch){

        for(int i = 0; i < ch.length; i++){
            for (int j = 0; j < ch.length; j++){
                if (ch[i] < ch[j]){
                    char temp = ch[i];
                    ch[i] = ch[j];
                    ch[j] = temp;
                }
            }
        }

        return ch;
    }

    public static char[] bubbleSortCharDesc(char[] ch){

        for(int i = 0; i < ch.length; i++){
            for (int j = 0; j < ch.length; j++){
                if (ch[i] > ch[j]){
                    char temp = ch[i];
                    ch[i] = ch[j];
                    ch[j] = temp;
                }
            }
        }

        return ch;
    }


}
