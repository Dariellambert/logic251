package com.xa.pr;

import java.util.Scanner;

public class SearchMain {

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        soal05(scan);
    }

    public static void soal05(Scanner scan){
        int selisih = scan.nextInt();
        String[] input = scan.nextLine().split(",");

        int []num = new int[input.length];

        for (int i = 0; i < input.length; i++){
            num[i] = Integer.parseInt(input[i]);
        }

        int count =0;
        for (int i = 0; i < num.length; i++){
            for(int j= 0; j <num.length; j++){
                if ( num[i] - num[j] == selisih ){
                 count++;
                }
            }
        }
        System.out.println(count);
    }

}
