package com.xa.pr;

import java.util.Arrays;
import java.util.Scanner;

public class SortMain {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);

        //soal02(scan);
        //soal02alt(scan);

        char[] huruf = soal02alt(scan);
        cetak(new String(huruf));


        //cetak(huruf);
        //soal03(scan);
    }

    public static void cetak(Object obj){
        System.out.println(obj);
    }

    public static void soal02(Scanner scan){
        System.out.print("Masukan huruf: ");
        String input = scan.nextLine();
        String[] pisah = input.split(",");

        String temp ="";
        for (int i = 0; i < pisah.length; i++){
            temp += pisah[i];
        }
        //System.out.println(temp);
        char[] huruf  = temp.toCharArray();
        Arrays.sort(huruf);

        for (char c: huruf)
            System.out.println(c);

    }

    public static char[] soal02alt(Scanner scan){
        System.out.print("Masukan huruf: ");
        String input = scan.nextLine();
        String[] pisah = input.split(",");

        String temp ="";
        for (int i = 0; i < pisah.length; i++){
            temp += pisah[i];
        }
        //System.out.println(temp);
        char[] huruf  = temp.toCharArray();

        for (int i =0; i < huruf.length; i++){
            for (int j = 0; j < huruf.length; j++){
                if (huruf[i] < huruf[j]){
                    char tamp = huruf[i];
                    huruf[i] = huruf[j];
                    huruf[j] = tamp;
                }
            }
        }

        return huruf;

    }

    public static void soal03(Scanner scan){
        System.out.print("Masukan angka:");
        String input = scan.nextLine();
        String[] pisah = input.split(",");
        int[] angka = new int[pisah.length];

        for (int i = 0; i <angka.length; i++){
            angka[i] = Integer.parseInt(pisah[i]);
        }

        for (int i = 0; i < angka.length; i++){
            for (int j =0; j < angka.length; j++){
                if(angka[i] < angka[j]){
                    int temp = angka[i];
                    angka[i] = angka[j];
                    angka[j] = temp;
                }
            }

        }

        for (int a: angka)
            System.out.print(angka[a-1]+" ");

        if(angka.length % 2 ==0)
            System.out.println("medianya adalah: "+ angka[(angka.length/2)-1]);
        else
            System.out.println("medianya adalah: "+ angka[angka.length/2]);


    }
}
