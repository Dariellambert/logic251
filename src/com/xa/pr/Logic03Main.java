package com.xa.pr;

public class Logic03Main {
    final static int n = 7;

    public static void main(String[] args) {
        System.out.println("hello");
        //printMatrix();
        printMatrix2();

    }

    public static void printMatrix(){
        int[][] matrix= new int[n][n];
        int angka = 0;
        for (int i =0; i <n; i++){
            for (int j =0; j < n; j++){
                if(i > 0 && j < n-1 && i < n-1 && j > 0){
                    System.out.print("\t");
                }
                else{
                    matrix[i][j] += angka;
                    System.out.print(matrix[i][j]+"\t");
                }
                angka++;
            }
            System.out.println("");
            angka -= n-1;
        }
    }
    public static void printMatrix2(){
        int[][] matrix= new int[n][n];
        int[][] matrixTemp = new int[n+1][n+1];
        String totalRow = "";
        int totalCollumn = 0;
        int totalMiring =0;
        int angka = 0;
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                matrix[i][j]+= angka;
                System.out.print(matrix[i][j]+"\t");
                angka++;
                totalCollumn += matrix[i][j];
                if (j == i){
                    totalMiring += matrix[i][j];
                }
            }
            System.out.println(totalCollumn);
            totalRow += totalCollumn + "\t";
            angka -= n-1;
            totalCollumn -= totalCollumn;

        }
        System.out.print(totalRow);
        System.out.println(totalMiring);
    }

}
