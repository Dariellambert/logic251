package com.xa.pr;

import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.*;
public class Logic04Main {

    public static void main (String [] args){
        Scanner scan = new Scanner(System.in);
        //soal01(scan);
        //soal02(scan);
        //soal03(scan);
        //soal04(scan);
        //soal05(scan);
    }

    public static void soal01 (Scanner scan){
        System.out.print("Input:");
        String text = scan.nextLine();
        String pisah = text.replaceAll(
                String.format("%s|%s|%s",
                        "(?<=[A-Z])(?=[A-Z][a-z])",
                        "(?<=[^A-Z])(?=[A-Z])",
                        "(?<=[A-Za-z])(?=[^A-Za-z])"
                ),
                " "
        );
        System.out.println("Pemisahan CamelCase: "+ pisah);
        StringTokenizer tokenizer = new StringTokenizer(pisah);
        int jmlKata = tokenizer.countTokens();
        System.out.println("jumlah kata: "+jmlKata);


    }

    public static void soal02(Scanner scan){
        System.out.print("Masukan kata: ");
        String text = scan.nextLine();
        String hasil ="";
        for (int i = 0; i < text.length(); i++){
            if((text.charAt(i)>= 48 && text.charAt(i) <= 57) ||(text.charAt(i) >= 65 && text.charAt(i) <= 90 )
                    || (text.charAt(i) >= 97 && text.charAt(i) <= 122) || text.charAt(i) == 32)
                hasil += Character.toString(text.charAt(i)+2);
            else
                hasil += Character.toString(text.charAt(i));
        }
        System.out.println(hasil.toLowerCase());

    }

    public static void soal03 (Scanner scan){
        System.out.print("input: ");
        String password = scan.nextLine();
        int strength = 0;
        if( password.matches("(?=.*[0-9]).*") )
                strength++;
        if( password.matches("(?=.*[a-z]).*") ||  password.matches("(?=.*[A-Z]).*")  )
            strength++;
        if( password.matches("(?=.*[A-Z]).*") )
            strength++;
        if( password.matches("(?=.*[~!@#$%^&*()_-]).*") )
            strength++;

        System.out.println("kekuatan sandi: " +strength);

    }



    public static void soal04 (Scanner scan){
        System.out.print("input: " );
        String message = scan.nextLine();
        String sos = "SOS";
        int countSos = 0;
        for (int i = 0; i < message.length(); i++) {
            if (message.charAt(i) != sos.charAt(i % 3))
                countSos++;
        }
        System.out.println("output: "+countSos);
    }

    public static void soal05 (Scanner scan){
        System.out.print("input: ");
        String angka = scan.nextLine();
        //String st2=angka.replaceAll("[^0-9]", "");

    }
}
