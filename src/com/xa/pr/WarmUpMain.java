package com.xa.pr;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class WarmUpMain {

    public static void main (String[] args){
        String replay = "";
        Scanner scan = new Scanner(System.in);

        do{
            System.out.print("Masukan no soal: ");
            int soal = scan.nextInt();
            switch (soal){
                case 8:
                    System.out.println("Soal no 1");
                    soal08aa(scan);
                    System.out.println(" ");
                    break;
                case 2:
                    System.out.println("Soal no 2");
                    soal02(scan);
                    System.out.println(" ");
                    break;
                case 3:
                    System.out.println("Soal no 3");
                    soal03(scan);
                    System.out.println(" ");
                    break;
                case 4:
                    System.out.println("Soal no 4");
                    soal04(scan);
                    System.out.println(" ");
                    break;
                case 5:
                    System.out.println("Soal no 5");
                    soal05(scan);
                    System.out.println(" ");
                    break;
                case 6:
                    System.out.println("Soal no 6");
                    soal06(scan);
                    System.out.println(" ");
                    break;
                case 7:
                    System.out.println("Soal no 7");
                    soal07(scan);
                    System.out.println(" ");
                    break;
                case 1:
                    System.out.println("Soal no 8");
                    soal01(scan);
                    System.out.println(" ");
                    break;
                case 9:
                    System.out.println("Soal no 9");
                    soal09(scan);
                    System.out.println(" ");
                    break;
                case 10:
                    System.out.println("Soal no 10");
                    soal10(scan);
                    System.out.println(" ");
                    break;
                default:
                    System.out.println("Inputan Salah");
                    break;
            }
            System.out.println("Pilih soal lagi? [Y/N]: ");
            replay = scan.next();

        }while (replay.equals("Y") || replay.equals("y"));
    }

    public static void soal01(Scanner scan){
        ArrayList <Integer> temp = new ArrayList<Integer>();
        System.out.println("Masukan jumlah elemen: ");
        int n = scan.nextInt();
        int [] angka = new int[n];
        for (int i =0; i < n; i++){
            angka[i] = scan.nextInt();
            temp.add(angka[i]);
        }

        int sum =0;
        for (int i =0; i < temp.size(); i++){
            sum += temp.get(i);
        }

        System.out.println("\nTotal nilai yg di input " +sum);
    }

    public static void soal02(Scanner scan){
        System.out.print("MAsukan ukuran matriks: ");
        int size = scan.nextInt();
        int [][] temp= new int[size][size];
        int diagL = 0;
        int diagR =0;

        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                System.out.print("masukan angka: ");
                temp[i][j] = scan.nextInt();
                if (i==j){
                    diagL += temp[i][j];
                }
                if (i+j == size-1){
                    diagR += temp[i][j];
                }
            }
        }

        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                System.out.print(temp[i][j]+ "\t");
            }
            System.out.println(" ");
        }

        int hasil = Math.abs(diagL - diagR);
        System.out.println("\nselisih dari serong kanan dan kiri adalah: "+hasil);
    }

    public static void soal03(Scanner scan){
        DecimalFormat df = new DecimalFormat("#0.00000");

        double pos =0;
        double neg = 0;
        double zero =0;
        System.out.println("Masukan jumlah deret: ");
        int deret = scan.nextInt();


        double [] num = new double[deret];

        for (int i = 0; i < deret; i++){
            System.out.print("masukan angka: ");
            num[i] = scan.nextInt();
            if (num[i] > 0)
                pos++;
            else if (num[i] < 0)
                neg++;
            else
                zero++;
        }
        System.out.println("\nJumlah presentase nilai positif: "+df.format(pos/deret));
        System.out.println("Jumlah presentase nilai negatif: "+df.format(neg/deret));
        System.out.println("Jumlah presentase nilai nol: "+df.format(zero/deret));
    }

    public static void soal04 (Scanner scan){
        System.out.println("Masukan ukuran tangga: ");
        int size = scan.nextInt();

        for (int i = 0; i < size; i++){
            for (int j =0; j < size; j++){
                if (i+j >= size-1)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println("");
        }
    }

    public static void soal05(Scanner scan){
        System.out.print("masukan deret: ");
        int n = scan.nextInt();

        ArrayList <Integer> num = new ArrayList<>();

        for (int i =0; i < n; i++){
            System.out.print("masukan angka: ");
            int value = scan.nextInt();
            num.add(value);
        }

        int min = num.get(0);
        int max = num.get(0);

        for (int nilai: num){
            if (nilai < min)
                min = nilai;

            if (nilai > max)
                max = nilai;
        }

        int sum =0;

        for (int i = 0; i < num.size(); i++ ){
            sum += num.get(i);
        }

        int maxResult = sum - min;
        int minResult = sum - max;

        System.out.println("\nHasilnya: "+minResult +" "+ maxResult);
    }

    public static void soal06(Scanner scan){
        System.out.print("Masukan umur: ");
        int umur = scan.nextInt();
        int max = 0;
        int tiup =0;
        ArrayList <Integer>lilin = new ArrayList<>();

        for (int i =0; i < umur; i++){
            System.out.print("masukan nilai panjang lilin: ");
            int pjgLilin = scan.nextInt();
            lilin.add(pjgLilin);
            if (lilin.get(i) > max)
                max =lilin.get(i);
        }

        for (int l: lilin){
            if(l == max)
                tiup++;
        }

        System.out.println("\n jumlah lilin tertinggi yg tertiup adalah "+tiup);
    }

    public static void soal07 (Scanner scan){
        String time = scan.next();
        String formatJam[] = time.split(":");
        String jam = formatJam[0];
        String menit = formatJam[1];
        String detik = formatJam[2].substring(0, 2);
        String suffix = formatJam[2].substring(2, 4);
        if(suffix.equals("AM")){
            if(jam.equals("12"))
                jam="00";

            System.out.println(jam+":"+menit+":"+detik);
        }else{
            if(!jam.equals("12")){
                int j = Integer.parseInt(jam);
                j +=12;
                jam =""+j;
            }
            System.out.println("\n konversi format 24h adalah: "+jam+":"+menit+":"+detik);
        }
    }

    public static void soal08aa(Scanner scan){
        System.out.print("Masukan kata: ");
        String text = scan.next();
        System.out.print("Masukan nilai enkripsi: ");
        int enkrip = scan.nextInt();
        String hasil ="";
        for (int i = 0; i < text.length(); i++){
            if( // cek angka ga kepake
                    (text.charAt(i) >= 65 && text.charAt(i) <= 90 ) // cek A-Z
                    || (text.charAt(i) >= 97 && text.charAt(i) <= 122)) // cek a-z
                    { // cek spasi ga kepake
                if ( text.charAt(i) >= 90 && text.charAt(i) < 97){ // cek lagi apakah lebih dari A-Z
                    hasil += Character.toString(text.charAt(i)%90+64+enkrip);
                }
                else if (text.charAt(i) >= 122 ) { // cek lagi apakah lebih dari a-z // 122%122 =0 +96 = mulai dari sblm a + nilai enkripsi
                    hasil += Character.toString(text.charAt(i)%122+96+enkrip);
                }
                else // kalo tidak lebih lsg ditambah enkripsinya aja
                    hasil += Character.toString(text.charAt(i)+enkrip);
            }
            else{ // bukan huruf, brarti tidak perlu di enripsi
                hasil += Character.toString(text.charAt(i));
            }
        }
        System.out.println("\nHasil Enkripsi Adalah: "+hasil);

    }

    public static void soal09 (Scanner scan){
        System.out.print("input: ");
        String password = scan.nextLine();
        int strength = 0;
        if( password.matches("(?=.*[0-9]).*") )
            strength++;
        if( password.matches("(?=.*[a-z]).*") ||  password.matches("(?=.*[A-Z]).*")  )
            strength++;
        if( password.matches("(?=.*[A-Z]).*") )
            strength++;
        if( password.matches("(?=.*[~!@#$%^&*()_-]).*") )
            strength++;

        System.out.println("\nkekuatan sandi: " +strength);
    }

    public static void soal10 (Scanner scan){
        System.out.print("input: " );
        String message = scan.nextLine();
        String sos = "SOS";
        int countNotSos = 0;
        for (int i = 0; i < message.length(); i++) {
            if (message.charAt(i) != sos.charAt(i % 3))
                countNotSos++;
        }
        System.out.println("\nReceived Signal tidak sinkron: "+countNotSos);
    }

}
