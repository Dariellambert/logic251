package com.xa.pr.oop;

import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main (String[] args){
        Rekening rek = new Rekening("1111", 10000, 123459876L);
        Rekening rek2 = new Rekening("1111", 10000, 7654321L);

        HashMap<Rekening, Long> mapRekening = new HashMap<Rekening, Long>();

        mapRekening.put(rek, rek.getNoRek());
        mapRekening.put(rek2, rek2.getNoRek());

        Scanner scan = new Scanner(System.in);

        doTransfer(scan,rek,rek2, mapRekening);

        System.out.println();

    }

    public static void doTransfer(Scanner scan, Rekening rekSend, Rekening rekReceive, HashMap<Rekening, Long> map){
        System.out.println("Masukan Password : ");
        String inputPassword = scan.nextLine();
        if (!inputPassword.equals(rekSend.getPassword())){
            System.out.println("Password salah");
            System.out.println("Anda penipu!!! rekening terblokir");
        }
        else{
            System.out.println("Masukan no rekening tujuan: ");
            long inputNoRek = scan.nextInt();

            if(!map.containsValue(inputNoRek)){
                System.out.println("Nomor rekening tidak ditemukan");
            }
            else {
                System.out.println("Masukan nominal transfer");
                int inputTransfer = scan.nextInt();
                System.out.println("No rekening tujuan: "+rekReceive.getNoRek());
                System.out.println("Nominal Transfer: "+inputTransfer);
                System.out.print("Konfirmasi transfer [Y/N]");
                String konfirmasi = scan.next();

                if (konfirmasi.equals("Y") || konfirmasi.equals("y")){
                    rekSend.transfer(inputTransfer, rekReceive);
                    System.out.println("\nTransaksi Berhasil");
                    cetakSaldo(rekSend,rekReceive);
                }
                else{
                    System.out.println("Transaksi gagal");
                }
            }
        }
    }

    public static void cetakSaldo (Rekening rekSend, Rekening rekReceive){
        System.out.println("Saldo anda: " +rekSend.getSaldo());
        System.out.println(rekReceive.getSaldo());
    }


}
