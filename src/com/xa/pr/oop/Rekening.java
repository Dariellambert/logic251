package com.xa.pr.oop;

public class Rekening {
    private String password;
    private int saldo;
    private long noRek;

    Rekening(String password, int saldo, long noRek){
        this.setPassword(password);
        this.setSaldo(saldo);
        this.setNoRek(noRek);

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public long getNoRek() {
        return noRek;
    }

    public void setNoRek(long noRek) {
        this.noRek = noRek;
    }

    public void transfer(int jumlah, Rekening rek){
        this.saldo -= jumlah;
        rek.saldo += jumlah;
    }


}
