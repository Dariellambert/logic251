package com.xa.pr;

import java.util.Scanner;

public class Logic07Main {
    //final static int n = 7;
    public static void main (String[] args){
        String replay = "";
        Scanner scan = new Scanner(System.in);
        do{
            System.out.print("Masukan no soal: ");
            int soal = scan.nextInt();
            switch (soal){
                case 1:
                    System.out.println("Soal no 1");
                    soal01(scan);
                    System.out.println(" ");
                    break;
                case 2:
                    System.out.println("Soal no 2");
                    soal02(scan);
                    System.out.println(" ");
                    break;
                case 3:
                    System.out.println("Soal no 3");
                    soal03(scan);
                    System.out.println(" ");
                    break;
                case 4:
                    System.out.println("Soal no 4");
                    soal04(scan);
                    System.out.println(" ");
                    break;
                case 5:
                    System.out.println("Soal no 5");
                    soal05(scan);
                    System.out.println(" ");
                    break;
                case 6:
                    System.out.println("Soal no 6");
                    soal06(scan);
                    System.out.println(" ");
                    break;
                case 7:
                    System.out.println("Soal no 7");
                    soal07(scan);
                    System.out.println(" ");
                    break;
                case 8:
                    System.out.println("Soal no 8");
                    soal08(scan);
                    System.out.println(" ");
                    break;
                case 9:
                    System.out.println("Soal no 9");
                    soal09(scan);
                    System.out.println(" ");
                    break;
                default:
                    System.out.println("Inputan Salah");;
                    break;
            }
            System.out.println("Pilih soal lagi? [Y/N]: ");
            replay = scan.next();

        }while (replay.equals("Y") || replay.equals("y"));


    }

    public static void soal01(Scanner scan){
        System.out.print("masukan panjang himpunan: ");
        int n = scan.nextInt();
        int num = 9;

        for (int i=0; i < n; i++){
            System.out.print(num+" ");
            num-=2;
        }
    }

    public static void soal02(Scanner scan){
        System.out.print("masukan panjang himpunan: ");
        int n = scan.nextInt();
        int num = 1;
        int diff = 1;

        for (int i=0; i < n; i++){
            System.out.print(num+" ");
            num+= diff;
            diff++;
        }
    }

    public static void soal03(Scanner scan){
        System.out.print("masukan panjang himpunan: ");
        int n = scan.nextInt();
        long num = 1L;

        for (int i=0; i < n; i++){
            System.out.print(num+" ");
            num*=n;
        }
    }

    public static void soal04(Scanner scan){
        System.out.print("masukan panjang himpunan: ");
        int n = scan.nextInt();
        int num = 0;
        int reset = 0;
        System.out.print("masukan jumlah baris: ");
        int jmlBaris = scan.nextInt();


        for (int i=0; i < jmlBaris; i++){
            for (int j =0; j <n; j++){
                System.out.print(num+"\t");
                num+=jmlBaris;
            }
            System.out.println(" ");
            if (num > n*jmlBaris-1){
                num  = reset + 1;
                reset++;
            }
        }
    }

    public static void soal05(Scanner scan){
        System.out.print("masukan panjang himpunan: ");
        int n = scan.nextInt();
        int num = 0;
        int tengah = (n/2)+1;
        int numBaru = 0;
        int[][] temp = new int[2][n];


        for(int i = 0; i < n; i++){
            for (int j = 0; j < 2; j++){
                temp[j][i] += num;
                if (num == n) break;
                num++;
            }
        }

        numBaru = num - 2;
        for (int i =tengah; i < n; i++){
            for (int j = 1; j >= 0; j--){
                temp[j][i] = numBaru;
                numBaru--;
            }
        }

        for (int i = 0; i < 2; i++){
            for( int j = 0; j < n; j++){
                System.out.print(temp[i][j]+ "\t");
            }
            System.out.println(" ");
        }
    }

    public static void soal06(Scanner scan){
        System.out.print("masukan panjang himpunan: ");
        int n = scan.nextInt();
        int num =1;
        int [][] temp = new int[2][n];
        int sum = 0;


        for (int i = 0; i < 2; i++){
            for (int j = 0; j < n; j++){
                if (i == 0){
                    temp [i][j] = num;
                    System.out.print(temp [i][j]+ "\t");
                    num++;
                    sum += temp[0][j];
                }
                else if (i > 0){
                    temp [i][j] = sum-1;
                    System.out.print(temp [i][j]+ "\t");
                    sum--;
                }
            }
            System.out.println(" ");
        }
    }

    public static void soal07(Scanner scan){
        System.out.print("masukan panjang himpunan: ");
        int n = scan.nextInt();
        int num = 1;
        System.out.print("masukan jarak: ");
        int dif = scan.nextInt();
        int jmlBaris = 3;
        int [][] temp  = new int[jmlBaris][n] ;

        for (int i =0; i < jmlBaris; i++){
            for (int j =0; j < n; j++ ){

                if (i == 0){
                    System.out.print(num+"\t");
                    temp[i][j] += num;
                    num+=dif;
                }
                else if (i>0) {
                    System.out.print((n*i) * temp[i-1][j]+ "\t");
                }
            }
            System.out.println(" ");
        }
    }

    public static void soal08(Scanner scan){
        System.out.print("masukan panjang himpunan: ");
        int n = scan.nextInt();
        int num = 1;
        int num2 =3;
        int dif = 3;
        int jmlBaris = 2;
        int countKelipatan = 0;
        int [][] temp  = new int[jmlBaris][n] ;

        for (int i =0; i < jmlBaris; i++){
            for (int j =0; j < n; j++ ){

                if (i == 0){
                    if ((j+1)% 3 == 0){
                        countKelipatan++;
                        System.out.print(dif * (num2) * countKelipatan +"\t");
                        num++;
                    }
                    else {
                        System.out.print(num+"\t");
                        temp[i][j] += num;
                        num++;
                    }
                }
                else if (i>0) {
                    if ((j+1)% 3 == 0){
                        countKelipatan = 0;
                        countKelipatan++;
                        System.out.print(dif * countKelipatan +"\t");
                        num2 += dif;
                    }
                    else {
                        System.out.print(num2 + "\t");
                        num2 += dif;
                    }
                }

            }
            System.out.println(" ");
        }
    }

    public static void soal09(Scanner scan){
        System.out.print("masukan panjang himpunan: ");
        int size = scan.nextInt();
        int num = 1;
        //int size = 4;
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++ ){
                System.out.print(num+"\t");
                num++;
            }
            System.out.println(" ");
        }
    }

}
