package com.xa.pr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class LatihanMain {
    public static void main(String[] args) {
        soal07();
        soal01();
        System.out.println("\n");
        soal02();
        System.out.println("\n");
        soal03();
        System.out.println("\n");
        soal04();
        System.out.println("\n");
        soal05();
        System.out.println("\n");
        soal06();
        System.out.println("\n");
        soal07();
        System.out.println("\n");
        soal08();
        System.out.println("\n");
        soal09();
        System.out.println("\n");
        soal10();
    }

    public static void soal02() {
        System.out.println("Soal 02");
        String str;
        int upperCount = 0;
        Scanner scan = new Scanner(System.in);


        System.out.print("input:  ");
        str = scan.nextLine();
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch >= 65 && ch <= 90) {
                upperCount++;
            }
        }
        System.out.println("output: " + upperCount);
    }

    public static void soal01() {
        System.out.println("Soal 01");
        Scanner scan = new Scanner(System.in);
        System.out.print("input: ");
        String str = scan.nextLine();
        ArrayList<String> token = new ArrayList<String>();

        System.out.print("output: ");

        StringTokenizer defaultTokenizer = new StringTokenizer(str);
        int jmlKata = defaultTokenizer.countTokens();

        for (int i = 0; i < jmlKata; i++) {
            token.add(defaultTokenizer.nextToken());
        }

        for (String kata : token) {
            String newKata = "";
            for (int i = 0; i < kata.length(); i++) {
                if (i != 0 && i != kata.length() - 1) {
                    newKata += "*";
                } else {
                    newKata += Character.toString(kata.charAt(i));
                }
            }
            System.out.print(newKata + " ");
        }
    }

    public static void soal04() {
        System.out.println("Soal 04");
        Scanner scan = new Scanner(System.in);
        System.out.print("input: ");
        String str = scan.nextLine();
        ArrayList<String> token = new ArrayList<String>();

        System.out.print("output: ");

        StringTokenizer defaultTokenizer = new StringTokenizer(str);
        int jmlKata = defaultTokenizer.countTokens();

        for (int i = 0; i < jmlKata; i++) {
            token.add(defaultTokenizer.nextToken());
        }

        for (String kata : token) {
            String newKata = "";
            for (int i = 0; i < kata.length(); i++) {
                if (i != kata.length() - 1) {
                    newKata += "*";
                } else {

                    newKata += Character.toString(kata.charAt(i));
                }
            }
            System.out.print(newKata.toUpperCase() + " ");
        }
    }

    public static void soal03() {
        System.out.println("Soal 03");
        Scanner scan = new Scanner(System.in);
        System.out.print("input: ");
        String str = scan.nextLine();
        ArrayList<String> token = new ArrayList<String>();

        System.out.print("output: ");

        StringTokenizer defaultTokenizer = new StringTokenizer(str);
        int jmlKata = defaultTokenizer.countTokens();

        for (int i = 0; i < jmlKata; i++) {
            token.add(defaultTokenizer.nextToken());
        }

        for (String kata : token) {
            String newKata = "";
            for (int i = 0; i < kata.length(); i++) {
                if (i != 0) {
                    newKata += "*";
                } else {
                    newKata += Character.toString(kata.charAt(i));
                }
            }
            System.out.print(newKata.toUpperCase() + " ");
        }
    }

    public static void soal05() {
        System.out.println("Soal 05");
        Scanner scan = new Scanner(System.in);
        System.out.print("input: ");
        String str = scan.nextLine();
        ArrayList<String> token = new ArrayList<String>();

        System.out.print("output: ");

        StringTokenizer defaultTokenizer = new StringTokenizer(str);
        int jmlKata = defaultTokenizer.countTokens();

        for (int i = 0; i < jmlKata; i++) {
            token.add(defaultTokenizer.nextToken());
        }

        for (String kata : token) {
            String newKata = "";
            int tengah = kata.length() / 2;
            newKata += Character.toString(kata.charAt(tengah));

            System.out.print(newKata.toUpperCase() + "***" + " ");
        }
    }

    public static void soal06() {
        System.out.println("Soal 06");
        Scanner scan = new Scanner(System.in);
        System.out.print("input: ");
        String str = scan.nextLine();
        ArrayList<String> token = new ArrayList<String>();

        System.out.print("output: ");

        StringTokenizer defaultTokenizer = new StringTokenizer(str);
        int jmlKata = defaultTokenizer.countTokens();

        for (int i = 0; i < jmlKata; i++) {
            token.add(defaultTokenizer.nextToken());
        }

        for (String kata : token) {
            String newKata = "";
            for (int i = 0; i < kata.length(); i++) {
                if (i >= (kata.length() / 2)) {
                    newKata += Character.toString(kata.charAt(i));
                }

            }
            System.out.print(newKata.toUpperCase() + " ");
        }
    }

    public static void soal07(){
        System.out.println("Soal 07");
        int count = 0;
        Scanner scan = new Scanner(System.in);
        System.out.print("input: ");
        String str = scan.nextLine();
        for (int i = 0; i < str.length(); i++){
            if((str.charAt(i)>= 48 && str.charAt(i) <= 57) ||(str.charAt(i) >= 65 && str.charAt(i) <= 90 )
                    || (str.charAt(i) >= 97 && str.charAt(i) <= 122) || str.charAt(i) == 32)
                count+= 0;
            else
                count++;
        }
        System.out.println("Output: "+count);
    }

    public static void soal08(){
        System.out.println("Soal 08");
        String awal, balik = "";
        Scanner in = new Scanner(System.in);
        System.out.print("Input: ");
        awal = in.nextLine();

        //int length = awal.length();

        for (int i = awal.length() - 1; i >= 0; i--)
            balik += Character.toString(awal.charAt(i));

        System.out.println("Output: "+balik);
        if (awal.equals(balik))
            System.out.println("WaW Palindrome");
    }

    public static void soal09(){
        System.out.println("Soal 09");
        Scanner scan = new Scanner(System.in);
        System.out.print("input: ");
        String str = scan.nextLine();
        ArrayList<String> token = new ArrayList<String>();

        System.out.print("output: ");

        StringTokenizer defaultTokenizer = new StringTokenizer(str);
        int jmlKata = defaultTokenizer.countTokens();

        for (int i = 0; i < jmlKata; i++) {
            token.add(defaultTokenizer.nextToken());
        }

        for (String kata : token) {
            String newKata = "";
            char[] sortKata =kata.toLowerCase().toCharArray();
            Arrays.sort(sortKata);
            newKata += String.valueOf(sortKata);
            System.out.print(newKata + " ");
        }
    }

    public static void soal10() {
        System.out.println("Soal 10");
        Scanner scan = new Scanner(System.in);
        System.out.print("input: ");
        String str = scan.nextLine();
        ArrayList<String> token = new ArrayList<String>();

        System.out.print("output: ");

        StringTokenizer defaultTokenizer = new StringTokenizer(str);
        int jmlKata = defaultTokenizer.countTokens();

        for (int i = 0; i < jmlKata; i++) {
            token.add(defaultTokenizer.nextToken());
        }

        for (String kata : token) {
            String newHurufAwal = "";
            String newHurufAkhir = "";
            for (int i = 0; i < kata.length(); i++) {
                if (i == 0) {
                    newHurufAwal += Character.toString(kata.charAt(i));
                }
                if (i == kata.length() - 1) {
                    newHurufAkhir += Character.toString(kata.charAt(i));
                }
            }
            System.out.print(newHurufAwal.toLowerCase() + "***"+ newHurufAkhir.toLowerCase()+ " ");
        }


    }
}
