select  	c.country_name, d.department_id, d.department_name, count (e.employee_id) as total_employee
from		employees e
inner join	departments d
on			e.department_id = d.department_id
Inner join  locations l
On			d.location_id = l.location_id
Inner Join 	countries c
On			l.country_id = c.country_id
group by	d.department_id, l.location_id, c.country_id
order by	c.country_name asc