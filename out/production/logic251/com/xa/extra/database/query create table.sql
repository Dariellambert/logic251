CREATE TABLE public.tblm_agama
(
    id_agama_pk 	serial primary key,
    kode_agama 		character(5),
    deskripsi 		character varying(20),
    is_active 		bit NOT NULL,
    created_by 		character varying(50),
    created_date 	date,
    updated_by 		character varying(50),
    updated_date	date
);

CREATE TABLE public.tblm_type_dosen
(
    id_type_dosen_pk 	serial primary key,
    kode_type_dosen 	character(5),
    deskripsi 		character varying(20),
    is_active 		bit NOT NULL,
    created_by 		character varying(50),
    created_date	date,
    updated_by 		character varying(50),
    updated_date 	date
);

CREATE TABLE public.tblm_jurusan
(
    id_jurusan_pk 	serial primary key,
    kode_jurusan 	character(5),
    nama_jurusan 	character varying(50),
    is_active 		bit NOT NULL,
    "created_by" 	character varying(50),
    created_date 	date,
    updated_by 		character varying(50),
    updated_date 	date
    
);

CREATE TABLE public.tblm_dosen
(
    id_dosen_pk 		bigserial primary key,
    kode_dosen 			character(5),
    nama_dosen 			character varying(100),
    id_jurusan_fk 		integer,
    id_type_dosen_fk 		integer,
    is_active 			bit NOT NULL,
    created_by 			character varying(50),
    created_date 		date,
    updated_by 			character varying,
    updated_date 		date
);

CREATE TABLE public.tblm_mahasiswa
(
    id_mahasiswa_pk 		bigserial primary key,
    kode_mahasiswa 		character(5),
    nama_mahasiswa 		character varying(100),
    alamat 			character varying(200),
    id_agama_fk 		integer,
    id_jurusan_fk 		integer,
    is_active 			bit NOT NULL,
    created_by 			character varying(50),
    created_date 		date,
    updated_by 			character varying(50),
    updated_date 		date
);

CREATE TABLE public.tblr_ujian
(
    id_ujian_pk 	serial primary key,
    kode_ujian 		character(5),
    nama_ujian 		character varying(50),
    is_active 		bit NOT NULL,
    created_by 		character varying(50),
    created_date 	date,
    updated_by 		character varying,
    updated_date 	date
);

CREATE TABLE public.tblt_nilai
(
    id_nilai_pk 	serial primary key,
    id_mahasiswa_fk 	bigint,
    id_ujian_fk 	integer,
    nilai 		decimal(8,2),
    is_active 		bit,
    created_by 		character varying(50),
    created_date 	date,
    updated_by 		character varying,
    updated_date 	date
);
